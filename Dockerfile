FROM python:3.8-slim
RUN python -m pip install --upgrade pip
RUN apt-get update \
    && apt-get -y install libpq-dev gcc 
COPY requirements.txt requirements.txt
RUN python -m pip install -r requirements.txt
COPY . .
